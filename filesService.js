// requires
const path = require('path');
const fs = require('fs');

// constants
const directoryPath = path.join(__dirname, 'files');
const JsonFile = 'fileWithPasswords.json';
let filePass = {};

function createFile (req, res, next) {
  const { filename, content } = req.body;
  const password = req.query.pass;

  if (filename === '' || content === '' || !filename || !content) {
    return res.status(400).send({ message: "Please specify 'content' parameter" });
  }

  fs.writeFile(path.join(directoryPath, filename), content, (err) => {
    if(err) {
      res.status(400).send({
        message: 'Client error',
      });

      return;
    }

    res.status(200).send({ "message": "File created successfully" });

   if (password) {

      filePass[filename] = password;

      fs.writeFile(path.join(__dirname, JsonFile), JSON.stringify(filePass), { flag: "a" } ,(err) => {
        return res.status(400).send({ message: "Client error" });
      });
    }
  });

}

function getFiles (req, res, next) {

  fs.readdir(directoryPath, function (err, files) {
    //handling error
    if (err) {
      res.status(400).send({
        message: 'Client error',
      });

      return;
    }

    res.status(200).send({
      "message": "Success",
      "files": files });
  });

}

const getFile = (req, res, next) => {
  const { filename } = req.params;

  try {
    const content = fs.readFileSync(path.join(directoryPath, filename), 'utf8');
    const extName = path.extname(filename).slice(1, filename.length - 1);
    const { birthtime } = fs.statSync(path.join(directoryPath, filename));

    res.status(200).send({
      "message": "Success",
      "filename": filename,
      "content": content,
      "extension": extName,
      "uploadedDate": birthtime });

  } catch (e) {
    res.status(400).send({ "message": `No file with '${filename}' filename found` });
  }

}

const editFile = (req, res, next) => {
  const { filename } = req.params;
  const { content } = req.body;

  try {
    fs.writeFile(path.join(directoryPath, filename),content ,{
      encoding: "utf8",
      flag: "a"
    } ,(err) => {
      if(err) {
        res.status(400).send({
          message: 'Client error',
        });

        return;
      }

      res.status(200).send({ "message": "File updated successfully" });
    });
  } catch (e) {

  }
}

const deleteFile = (req, res, next) => {
  const { filename } = req.params;

  try {
    fs.unlinkSync(`${directoryPath}\\${filename}`);

    res.status(200).send({ "message": `Success. File ${filename} was removed successfully` });
  } catch(err) {
    res.status(400).send({ "message": `Cannot delete '${filename}'` });
  }
}

// path.extName('file.txt') ---> '.txt'
// fs.writeFile ({ flag: 'a' }) ---> adds content to the file

module.exports = {
  createFile,
  getFiles,
  getFile,
  editFile,
  deleteFile
}
